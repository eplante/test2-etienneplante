package question_two;

public class UnionizedHourlyEmployee extends HourlyEmployee {
	public double pensionContribution;
	
	public UnionizedHourlyEmployee(int weekWorkHours, double hourlyPay, double pensionContribution) {
		super(weekWorkHours, hourlyPay);
		this.pensionContribution = pensionContribution;
	}
	
	@Override
	public double getYearlyPay() {
		// TODO Auto-generated method stub
		return weekWorkHours * hourlyPay * 52 + pensionContribution;
	}
}
