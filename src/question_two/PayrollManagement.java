package question_two;

public class PayrollManagement {

	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(100000.00);
		employees[1] = new SalariedEmployee(72300.00);
		employees[2] = new HourlyEmployee(30, 17.50);
		employees[3] = new UnionizedHourlyEmployee(30, 21.15, 1000);
		employees[4] = new UnionizedHourlyEmployee(48, 21.15, 1500);
		
		System.out.println(getTotalExpenses(employees) + "$");
	}
	
	public static double getTotalExpenses(Employee[] employees)
	{
		double expenses = 0;
		for(Employee e : employees) {
			expenses += e.getYearlyPay();
		}
	
		return expenses;
	}
}
