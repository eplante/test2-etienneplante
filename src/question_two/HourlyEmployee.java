package question_two;
public class HourlyEmployee implements Employee {
	protected int weekWorkHours;
	protected double hourlyPay;

	public HourlyEmployee(int weekWorkHours, double hourlyPay)
	{
		this.weekWorkHours = weekWorkHours;
		this.hourlyPay = hourlyPay;
	}
	
	@Override
	public double getYearlyPay() {
		// TODO Auto-generated method stub
		return weekWorkHours * hourlyPay * 52;
	}

}
