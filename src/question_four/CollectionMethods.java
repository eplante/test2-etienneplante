package question_four;

import java.util.ArrayList;
import java.util.Collection;
import question_three.Planet;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets)
	{
		Collection<Planet> firstThree = new ArrayList<Planet>();
		
		for(Planet planet : planets)
		{
			if(planet.getOrder() <= 3 && planet.getOrder() > 0) { firstThree.add(planet); }
		}
		
		return firstThree;
	}
}
